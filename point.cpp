#include "point.h"
#include <QPainter>
#include <QDebug>
#include <QtWidgets/qframe.h>
#include <QWidget>
#include <sys/time.h>
point::point(QWidget *parent):QLabel(parent)
{
}
void point::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
   //设置画笔颜色
    painter.setPen(QColor(31, 225, 228));

    QFont font;
    font.setFamily("Microsoft YaHei");
    font.setPointSize(20);
    font.setItalic(true);
    painter.setFont(font);

    // 绘制文本
    painter.drawText(rect(), Qt::AlignCenter, "画笔颜色");

    QLabel::paintEvent(event);

}
