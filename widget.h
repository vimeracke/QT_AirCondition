#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QListWidget>
#include <QString>
#include <qstring.h>
#include "qdebug.h"
#include <QDebug>
#include <QTimer>
#include <QThread>
#include <vector>
#include <QPainter>
#include <qpainter.h>

using namespace std;
namespace Ui {
    class Widget;
}

class Widget : public QWidget
{
Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

public:


    struct MemberInfo{
        char *pix_face;
        char *str_add_time;
        char *str_mode;
        char *str_temprature;
        char *str_spped;
        char *str_dir;

    };
public:
    QPixmap pix_background;
    QPixmap pix_logo;
    QPixmap pix_point;
    QPixmap pix_face;
    QPixmap pix_ok;
    QPixmap pix_off;
    QPixmap pix_mode_cold;
    QPixmap pix_mode_hot;
    QPixmap pix_mode_wind;
    QPixmap pix_mode_rmwet;
    QPixmap pix_mode_healthy;
    QPixmap pix_tempdown;
    QPixmap pix_tempup;
    QPixmap pix_speed1;
    QPixmap pix_speed2;
    QPixmap pix_speed3;
    QPixmap pix_speed4;
    QPixmap pix_wind_up;
    QPixmap pix_wind_down;

    QPixmap yellow_pix_ok;
    QPixmap yellow_pix_off;
    QPixmap yellow_pix_face;
    QPixmap yellow_pix_mode_cold;
    QPixmap yellow_pix_mode_hot;
    QPixmap yellow_pix_mode_wind;
    QPixmap yellow_pix_mode_rmwet;
    QPixmap yellow_pix_mode_healthy;
    QPixmap yellow_pix_tempdown;
    QPixmap yellow_pix_tempup;
    QPixmap yellow_pix_speed1;
    QPixmap yellow_pix_speed2;
    QPixmap yellow_pix_speed3;
    QPixmap yellow_pix_speed4;
    QPixmap yellow_pix_wind_up;
    QPixmap yellow_pix_wind_down;
    QTimer *timer;
public slots:
    void slot_timeout();
    void slot_flag(QTimer *);
signals:
    void signal_flag(QTimer *);

private:
    Ui::Widget *ui;
};
#endif // WIDGET_H
