#include "widget.h"
#include <QLayout>
#include <qstackedlayout.h>
#include <QStackedLayout>
#include "./build/ui_widget.h"
#include "QFont"
#include "QTimer"
#include "QTime"
#include "QObject"
#include "QThread"
#include "QString"
#include <QPropertyAnimation>
#include <QGraphicsOpacityEffect>
#include <QSequentialAnimationGroup>
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    pix_background = QPixmap("./icon/UI.png");
    pix_logo = QPixmap("./icon/logo.png");
    pix_point = QPixmap("./icon/point.png");
    pix_face = QPixmap("./icon/识别到人脸.png");
    pix_ok = QPixmap("./icon/夺取控制权.png");
    pix_off = QPixmap("./icon/u314.png");
    pix_mode_cold = QPixmap("./icon/制冷.png");
    pix_mode_hot = QPixmap("./icon/制暖.png");
    pix_mode_wind = QPixmap("./icon/送风.png");
    pix_mode_rmwet = QPixmap("./icon/抽湿.png");
    pix_mode_healthy = QPixmap("./icon/健康.png");
    pix_tempdown = QPixmap("./icon/调低温度.png");
    pix_tempup = QPixmap("./icon/调高温度.png");
    pix_speed1 = QPixmap("./icon/风速1.png");
    pix_speed2 = QPixmap("./icon/风速2.png");
    pix_speed3 = QPixmap("./icon/风速3.png");
    pix_speed4 = QPixmap("./icon/风速4.png");
    pix_wind_up = QPixmap("./icon/向上送风.png");
    pix_wind_down = QPixmap("./icon/向下送风.png");
/*====================================控制===================================*/

    yellow_pix_ok = QPixmap("./icon/icon_yellow/夺取控制权.png");
    yellow_pix_off = QPixmap("./icon/icon_yellow/u286.png");
    yellow_pix_face = QPixmap("./icon/icon_yellow/识别到人脸.png");
    yellow_pix_mode_cold = QPixmap("./icon/icon_yellow/制冷.png");
    yellow_pix_mode_hot = QPixmap("./icon/icon_yellow/制暖.png");
    yellow_pix_mode_wind = QPixmap("./icon/icon_yellow/送风.png");
    yellow_pix_mode_rmwet = QPixmap("./icon/icon_yellow/抽湿.png");
    yellow_pix_mode_healthy = QPixmap("./icon/icon_yellow/健康.png");
    yellow_pix_tempdown = QPixmap("./icon/icon_yellow/调低温度.png");
    yellow_pix_tempup = QPixmap("./icon/icon_yellow/调高温度.png");
    yellow_pix_speed1 = QPixmap("./icon/icon_yellow/风速1.png");
    yellow_pix_speed2 = QPixmap("./icon/icon_yellow/风速2.png");
    yellow_pix_speed3 = QPixmap("./icon/icon_yellow/风速3.png");
    yellow_pix_speed4 = QPixmap("./icon/icon_yellow/风速4.png");
    yellow_pix_wind_up = QPixmap("./icon/icon_yellow/向上送风.png");
    yellow_pix_wind_down = QPixmap("./icon/icon_yellow/向下送风.png");
/*===========================//调整图片的大小和控件一致============================*/

    pix_background         = pix_background.scaled(ui->background->size());
    pix_logo               = pix_logo.scaled(ui->logo->size());
    pix_ok                 = pix_ok.scaled(ui->ok->size());
    pix_off                = pix_off.scaled(ui->ok->size());
    pix_mode_cold          = pix_mode_cold.scaled(ui->mode->size());
    pix_mode_hot           = pix_mode_hot.scaled(ui->mode->size());
    pix_mode_wind          = pix_mode_wind.scaled(ui->mode->size());
    pix_mode_rmwet         = pix_mode_rmwet.scaled(ui->mode->size());
    pix_tempdown           = pix_tempdown.scaled(ui->tempdown->size());
    pix_point              = pix_point.scaled(ui->point->size());
    pix_tempup             = pix_tempup.scaled(ui->tempup->size());
    pix_speed1             = pix_speed1.scaled(ui->speed->size());
    pix_speed2             = pix_speed2.scaled(ui->speed->size());
    pix_speed3             = pix_speed3.scaled(ui->speed->size());
    pix_speed4             = pix_speed4.scaled(ui->speed->size());
    pix_wind_up            = pix_wind_up.scaled(ui->wind->size());
    pix_wind_down          = pix_wind_down.scaled(ui->wind->size());
    pix_face               = pix_face.scaled(ui->face->size());
    yellow_pix_ok          = yellow_pix_ok.scaled(ui->ok->size());
    yellow_pix_off         = yellow_pix_off.scaled(ui->ok->size());
    yellow_pix_face        = yellow_pix_face.scaled(ui->face->size());
    yellow_pix_mode_cold   = yellow_pix_mode_cold.scaled(ui->mode->size());
    yellow_pix_mode_hot    = yellow_pix_mode_hot.scaled(ui->mode->size());
    yellow_pix_mode_wind   = yellow_pix_mode_wind.scaled(ui->mode->size());
    yellow_pix_mode_rmwet  = yellow_pix_mode_rmwet.scaled(ui->mode->size());
    yellow_pix_tempdown    = yellow_pix_tempdown.scaled(ui->tempdown->size());
    yellow_pix_tempup      = yellow_pix_tempup.scaled(ui->tempup->size());
    yellow_pix_speed1      = yellow_pix_speed1.scaled(ui->speed->size());
    yellow_pix_speed2      = yellow_pix_speed2.scaled(ui->speed->size());
    yellow_pix_speed3      = yellow_pix_speed3.scaled(ui->speed->size());
    yellow_pix_speed4      = yellow_pix_speed4.scaled(ui->speed->size());
    yellow_pix_wind_up     = yellow_pix_wind_up.scaled(ui->wind->size());
    yellow_pix_wind_down   = yellow_pix_wind_down.scaled(ui->wind->size());

/*==============================================================================*/

    ui->listWidget->setFrameShape(QListWidget::NoFrame);
    QPixmap head = QPixmap("./icon/1.jpeg");

    QString str_data;

//    ui->ok->setText("12℃");

    QFont ft;

    ft.setPointSize(22);
    ft.setFamily("ubuntu");
//    ui->ok->setFont(ft);
    //设置颜色
    QPalette pa;
    pa.setColor(QPalette::WindowText,QColor(31,225,228));
//    ui->ok->setPalette(pa);

    str_data =str_data +"\t  99" + "%";
    for(int i = 0;i<5;i++)
    {
        ui->listWidget_face->addItem(new QListWidgetItem(head,str_data));

    }
    ui->listWidget_face->setStyleSheet("QListWidget{border:0px solid gray; color:black; font: 18pt;}"
                                       "QListWidget::Item{padding-top:0px; padding-bottom:5px; margin-bottom:1px; }"
                                       "QListWidget::Item:hover{background:skyblue; }"
                                       "QListWidget{color:}"
                                       "QListWidget::item:selected{background:lightgray;}"
                                       "QListWidget::item:selected:!active{border-width:0px;background:#888A85;color:black }"
                                       "ScrollBarPolicy{width:0;height:0; }"
        );
    ui->listWidget_face->viewport()->setStyleSheet("background: #888A85");
    ui->listWidget->viewport()->setStyleSheet("background: #888A85");
    int length =1;
    QString debug = QString::number(length);

    ui->logo->setText(debug);
    QString str_test;
    QString str_compare;
    vector<double> a(5); //初始化size为10可以避免数组动态增长的时候不断的分配内存
    //v2.reserve(10);//同上，只要使用其中一个就可以了
    for( int i=91; i<95; i++ )
    {
        a.push_back(a[i]);//增加一个元素
    }

    for(vector<double>::iterator it=a.begin(); it!=a.end(); it++) {
            str_compare = QString::number(*it,'f',10);  // f 表示非科学记数法  10表示小数点后保留10位
            str_test = str_test + "\n" + str_compare + "%";
        }
    for(int i = 0;i<4;i++)
    {
        ui->listWidget->addItem(new QListWidgetItem(head,str_test));

    }
    ui->listWidget->setStyleSheet("QListWidget{border:0px solid gray; color:black; font: 12pt;}"
                                       "QListWidget::Item{padding-top:0px; padding-bottom:5px; margin-bottom:1px; }"
                                       "QListWidget::Item:hover{background:skyblue; }"
                                       "QListWidget::item:selected{background:lightgray; color:green; }"
                                       "QListWidget::item:selected:!active{border-width:0px;background:#888A85;color:black }"
                                       "QListWidget::item{background:#888A85;}"
                                       "QListWidget::viewport{background:#888A85}"
                                       "ScrollBarPolicy{width:0;height:0; }"

        );
    ui->listWidget_face->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->listWidget_face->setIconSize(QSize(100,100));//指定图标大小
    ui->listWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->listWidget->setIconSize(QSize(100,100));//指定图标大小

    ui->background->setPixmap(pix_background);
    ui->face->setPixmap(pix_face);
    ui->speed->setPixmap(pix_speed3);
    ui->logo->setPixmap(pix_logo);
    ui->mode->setPixmap(pix_mode_cold);
    ui->wind->setPixmap(pix_wind_up);

//    ui->point->setPixmap(pix_point);
    ui->tempdown->setPixmap(pix_tempdown);
    ui->tempup->setPixmap(pix_tempup);

    ui->point->setPixmap(pix_point);
//    timer = new QTimer();
//    connect(timer,SIGNAL(timeout()),this,SLOT(slot_timeout()));
//    connect(this, SIGNAL(signal_flag(QTimer *)), this, SLOT(slot_flag(QTimer *)));
//    for(int i=0;i<5;i++)
//    {
//        emit signal_flag(timer);
//    }

//    ui->ok->setText("234124");
    QGraphicsOpacityEffect *m = new QGraphicsOpacityEffect(ui->point);
    m->setOpacity(1);
    ui->point->setGraphicsEffect(m);

    QPropertyAnimation *n1 = new QPropertyAnimation(m,"opacity",this);
    n1->setEasingCurve(QEasingCurve::Linear);
    n1->setDuration(1500);
    n1->setStartValue(1);
    n1->setEndValue(0);


    QPropertyAnimation *n2 = new QPropertyAnimation(m,"opacity",this);
    n2->setEasingCurve(QEasingCurve::Linear);
    n2->setDuration(1500);
    n2->setStartValue(0);
    n2->setEndValue(1);


    QSequentialAnimationGroup *group = new QSequentialAnimationGroup;
    group->addAnimation(n1);
    group->addAnimation(n2);
    group->start();
}

void Widget::slot_flag(QTimer * timer)
{
    qDebug()<<__LINE__<<endl;
    timer->start(250);
    timer->stop();

}

Widget::~Widget()
{
    delete ui;
}

void Widget::slot_timeout()
{
    if(timer->isActive())
    {
        qDebug()<<__LINE__<<endl;
    }
    else
    {
        qDebug()<<__LINE__<<endl;
    }

}


