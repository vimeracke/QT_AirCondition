/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QLabel *mode;
    QLabel *speed;
    QLabel *tempdown;
    QLabel *tempup;
    QLabel *wind;
    QLabel *ok;
    QLabel *face;
    QLabel *list_left;
    QLabel *list_right;
    QListWidget *listWidget;
    QListWidget *listWidget_face;
    QLabel *point;
    QLabel *logo;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *Layout;
    QLabel *label_info_left;
    QLabel *label_info_right;
    QLabel *background;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QStringLiteral("Widget"));
        Widget->resize(1920, 1080);
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(136, 138, 133, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(255, 255, 255, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush2);
        QBrush brush3(QColor(127, 127, 127, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush3);
        QBrush brush4(QColor(170, 170, 170, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush4);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush2);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        QBrush brush5(QColor(255, 255, 220, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush5);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        Widget->setPalette(palette);
        Widget->setLayoutDirection(Qt::LeftToRight);
        Widget->setStyleSheet(QStringLiteral(""));
        mode = new QLabel(Widget);
        mode->setObjectName(QStringLiteral("mode"));
        mode->setGeometry(QRect(825, 128, 51, 51));
        mode->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        speed = new QLabel(Widget);
        speed->setObjectName(QStringLiteral("speed"));
        speed->setGeometry(QRect(1039, 127, 51, 51));
        speed->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        tempdown = new QLabel(Widget);
        tempdown->setObjectName(QStringLiteral("tempdown"));
        tempdown->setGeometry(QRect(930, 200, 51, 51));
        tempdown->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        tempup = new QLabel(Widget);
        tempup->setObjectName(QStringLiteral("tempup"));
        tempup->setGeometry(QRect(930, 58, 51, 51));
        tempup->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        wind = new QLabel(Widget);
        wind->setObjectName(QStringLiteral("wind"));
        wind->setGeometry(QRect(1130, 131, 51, 51));
        wind->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        ok = new QLabel(Widget);
        ok->setObjectName(QStringLiteral("ok"));
        ok->setGeometry(QRect(926, 121, 61, 61));
        ok->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
""));
        face = new QLabel(Widget);
        face->setObjectName(QStringLiteral("face"));
        face->setGeometry(QRect(1262, 130, 51, 51));
        face->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        list_left = new QLabel(Widget);
        list_left->setObjectName(QStringLiteral("list_left"));
        list_left->setGeometry(QRect(10, 20, 451, 81));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        QBrush brush6(QColor(0, 154, 255, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush6);
        QBrush brush7(QColor(173, 189, 255, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush7);
        QBrush brush8(QColor(114, 140, 251, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush8);
        QBrush brush9(QColor(28, 46, 123, 255));
        brush9.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush9);
        QBrush brush10(QColor(37, 61, 165, 255));
        brush10.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush10);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush2);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush6);
        palette1.setBrush(QPalette::Active, QPalette::Shadow, brush);
        QBrush brush11(QColor(155, 173, 251, 255));
        brush11.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::AlternateBase, brush11);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipBase, brush5);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        QBrush brush12(QColor(32, 80, 218, 255));
        brush12.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush12);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush9);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush5);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush7);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        QBrush brush13(QColor(56, 92, 247, 255));
        brush13.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush13);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush5);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        list_left->setPalette(palette1);
        list_left->setStyleSheet(QStringLiteral("background-color: rgb(0, 154, 255);"));
        list_right = new QLabel(Widget);
        list_right->setObjectName(QStringLiteral("list_right"));
        list_right->setGeometry(QRect(1470, 20, 451, 81));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        palette2.setBrush(QPalette::Active, QPalette::Button, brush6);
        palette2.setBrush(QPalette::Active, QPalette::Light, brush7);
        palette2.setBrush(QPalette::Active, QPalette::Midlight, brush8);
        palette2.setBrush(QPalette::Active, QPalette::Dark, brush9);
        palette2.setBrush(QPalette::Active, QPalette::Mid, brush10);
        palette2.setBrush(QPalette::Active, QPalette::Text, brush);
        palette2.setBrush(QPalette::Active, QPalette::BrightText, brush2);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette2.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush6);
        palette2.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette2.setBrush(QPalette::Active, QPalette::AlternateBase, brush11);
        palette2.setBrush(QPalette::Active, QPalette::ToolTipBase, brush5);
        palette2.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush12);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush6);
        palette2.setBrush(QPalette::Inactive, QPalette::Light, brush7);
        palette2.setBrush(QPalette::Inactive, QPalette::Midlight, brush8);
        palette2.setBrush(QPalette::Inactive, QPalette::Dark, brush9);
        palette2.setBrush(QPalette::Inactive, QPalette::Mid, brush10);
        palette2.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::BrightText, brush2);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush6);
        palette2.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush11);
        palette2.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush5);
        palette2.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush9);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush6);
        palette2.setBrush(QPalette::Disabled, QPalette::Light, brush7);
        palette2.setBrush(QPalette::Disabled, QPalette::Midlight, brush8);
        palette2.setBrush(QPalette::Disabled, QPalette::Dark, brush9);
        palette2.setBrush(QPalette::Disabled, QPalette::Mid, brush10);
        palette2.setBrush(QPalette::Disabled, QPalette::Text, brush9);
        palette2.setBrush(QPalette::Disabled, QPalette::BrightText, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush9);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush6);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush6);
        palette2.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush13);
        palette2.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush5);
        palette2.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        list_right->setPalette(palette2);
        list_right->setStyleSheet(QLatin1String("background-color: rgb(0, 154, 255);\n"
"font: 20pt \"Ubuntu\";\n"
""));
        listWidget = new QListWidget(Widget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(10, 160, 451, 891));
        listWidget->setStyleSheet(QLatin1String("margin:-1px;\n"
"background-color: rgb(255, 255, 255);\n"
""));
        listWidget_face = new QListWidget(Widget);
        listWidget_face->setObjectName(QStringLiteral("listWidget_face"));
        listWidget_face->setGeometry(QRect(1470, 150, 451, 901));
        listWidget_face->setStyleSheet(QLatin1String("margin:-1px;\n"
"selection-color: rgb(136, 138, 133);\n"
"gridline-color: rgb(136, 138, 133);\n"
"border-color: rgb(136, 138, 133);\n"
"alternate-background-color: rgb(136, 138, 133);\n"
"selection-background-color: rgb(136, 138, 133);\n"
"background-color: rgb(136, 138, 133);"));
        point = new QLabel(Widget);
        point->setObjectName(QStringLiteral("point"));
        point->setGeometry(QRect(948, 189, 15, 15));
        point->setMinimumSize(QSize(10, 10));
        point->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        logo = new QLabel(Widget);
        logo->setObjectName(QStringLiteral("logo"));
        logo->setGeometry(QRect(594, 94, 171, 121));
        logo->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        horizontalLayoutWidget = new QWidget(Widget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(462, 319, 1001, 731));
        Layout = new QHBoxLayout(horizontalLayoutWidget);
        Layout->setSpacing(6);
        Layout->setContentsMargins(11, 11, 11, 11);
        Layout->setObjectName(QStringLiteral("Layout"));
        Layout->setContentsMargins(0, 0, 0, 0);
        label_info_left = new QLabel(Widget);
        label_info_left->setObjectName(QStringLiteral("label_info_left"));
        label_info_left->setGeometry(QRect(10, 99, 451, 61));
        label_info_left->setStyleSheet(QStringLiteral("font: 18pt \"DejaVu Sans\";"));
        label_info_right = new QLabel(Widget);
        label_info_right->setObjectName(QStringLiteral("label_info_right"));
        label_info_right->setGeometry(QRect(1470, 100, 451, 51));
        label_info_right->setStyleSheet(QStringLiteral("font: 18pt \"DejaVu Sans\";"));
        background = new QLabel(Widget);
        background->setObjectName(QStringLiteral("background"));
        background->setGeometry(QRect(472, -100, 1011, 471));
        background->setStyleSheet(QStringLiteral(""));
        background->raise();
        logo->raise();
        wind->raise();
        tempup->raise();
        tempdown->raise();
        face->raise();
        speed->raise();
        mode->raise();
        horizontalLayoutWidget->raise();
        label_info_right->raise();
        list_right->raise();
        label_info_left->raise();
        ok->raise();
        list_left->raise();
        listWidget_face->raise();
        listWidget->raise();
        point->raise();

        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", Q_NULLPTR));
        mode->setText(QString());
        speed->setText(QString());
        tempdown->setText(QString());
        tempup->setText(QString());
        wind->setText(QString());
#ifndef QT_NO_WHATSTHIS
        ok->setWhatsThis(QApplication::translate("Widget", "<html><head/><body><p><br/></p></body></html>", Q_NULLPTR));
#endif // QT_NO_WHATSTHIS
        ok->setText(QString());
        face->setText(QString());
        list_left->setText(QApplication::translate("Widget", "<html><head/><body><p align=\"center\"><span style=\" font-size:24pt;\">\345\256\266\345\272\255\346\210\220\345\221\230</span></p></body></html>", Q_NULLPTR));
        list_right->setText(QApplication::translate("Widget", "<html><head/><body><p align=\"center\"><span style=\" font-size:24pt;\">\351\235\242\351\203\250\344\277\241\346\201\257</span></p></body></html>", Q_NULLPTR));
        point->setText(QString());
        logo->setText(QString());
        label_info_left->setText(QApplication::translate("Widget", "   \345\244\264\345\203\217     \346\270\251\345\272\246      \346\250\241\345\274\217     \351\243\216\351\200\237     \351\243\216\345\220\221", Q_NULLPTR));
        label_info_right->setText(QApplication::translate("Widget", "           ID                        \347\233\270\344\274\274\345\272\246", Q_NULLPTR));
        background->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
