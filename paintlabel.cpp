#include "paintlabel.h"
#include <QPainter>
#include <QDebug>
#include <QtWidgets/qframe.h>
#include <QWidget>
#include <sys/time.h>
#include <QPoint>
PaintLabel::PaintLabel(QWidget *parent):QLabel(parent)
{
}
void PaintLabel::paintEvent(QPaintEvent *event)
{
    QPainter pain(this);
    pain.setPen(QColor(31, 225, 228));
    pain.setBrush(QBrush(QColor(31, 225, 228)));
    pain.drawEllipse(2,1,18,18);

    QLabel::paintEvent(event);

}
