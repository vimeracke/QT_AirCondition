#ifndef TEMP_H
#define TEMP_H

#include <QWidget>
#include <QLabel>
class temp : public QLabel
{
    Q_OBJECT
public:
    explicit temp(QWidget *parent = nullptr);
    void paintEvent(QPaintEvent *event);
signals:

public slots:
};

#endif // TEMP_H
