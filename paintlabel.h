#ifndef PAINTLABEL_H
#define PAINTLABEL_H

#include <QWidget>
#include <QLabel>
class PaintLabel : public QLabel
{
    Q_OBJECT
public:
    explicit PaintLabel(QWidget *parent = nullptr);
    void paintEvent(QPaintEvent *event);
    signals:

public slots:
};

#endif // PAINTLABEL_H
